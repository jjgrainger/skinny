<?php

require __DIR__ . '/../vendor/autoload.php';

use Slim\App;
use Slim\Views\Twig;
use Dopesong\Slim\Error\Whoops as WhoopsError;

// Create the slim app
$app = new App;

// Get container
$container = $app->getContainer();

// Create views with Twig
$container['view'] = function ($container) {

    $path = __DIR__ . '/templates';
    $view = new Twig($path);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

// Error Handling
$container['phpErrorHandler'] = $container['errorHandler'] = function($c) {
    return new WhoopsError($c->get('settings')['displayErrorDetails']);
};


// Require in routes
require __DIR__ . '/routes/web.php';
