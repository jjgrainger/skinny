<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Bootstrap the app
require __DIR__ . '/../app/boot.php';

// Run the app
$app->run();
